---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "1593527925"
subtitle: ""
summary: ""
authors: [carson-drake]
tags: ['Dailies']
categories: []
date: 2020-06-30T07:38:45-07:00
lastmod: 2020-06-30T07:38:45-07:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Driving family today, trying to closeout and organize open tabs... Need to reassess
prioritites and focus on learning implementing and not just consuming.

- [x] Driving
- [ ] Tab cleanup
- [ ] Figure out info diet
  