---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "1594263986"
subtitle: "turning tides"
summary: "the winds of war are shifting. time to take back what is mine"
authors: [carson-drake]
tags: ['Dailies']
categories: []
date: 2020-07-08T20:06:26-07:00
lastmod: 2020-07-08T20:06:26-07:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

## Back again

Just another day in the never ending struggle to establish some sense of normality.  

Who would ever think it could be (someone could make) getting some work done, doing some learing, and 
following a reasonably easy/healthy regimine of sleep and fitness...

Maybe im expecting the NSA or Russia to leave me a congradulator/supportive message in my inbox
recognizing my self inposed struggles and efforts...

## FORWARD

Anyways thats enough self loathing. time to actually make some progress.
