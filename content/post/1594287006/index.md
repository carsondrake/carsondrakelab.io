---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "1594287006"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2020-07-09T02:30:06-07:00
lastmod: 2020-07-09T02:30:06-07:00
featured: false
draft: false
highlight: true
diagram: true
# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Testing out mermaid diagrams

<!--more-->

```mermaid
graph BT
  c2_model & c1_model---central_db
  
  c1_raw ---c1_gph[("graph")]
  c2_raw ---c2_gph[("graph")]
  subgraph dde["dde"]
  central_db-.training process.- core_model
  end
  subgraph dde2["dde_sys2"]
  c2_gph-->c2_model-->|training process|c2_gph
  end
  subgraph dde1["dde_sys"]
  c1_gph-->c1_model-->|training process|c1_gph
  end
  
  c2_model & c1_model---core_model
  

  
```
  